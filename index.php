<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 13/11/2016
 * Time: 00:20
 */

use MojangAPI\AccountFactory;

require 'vendor/autoload.php';
require 'paths.php';

$account = AccountFactory::createByPlayerName('jeb_');
echo <<<HTML

    <img src="{$account->getSkin()->generateHead()->encode('data-url')}"/> 

HTML;
$account = AccountFactory::createByPlayerName('youtous');

echo <<<HTML

    <img src="{$account->getSkin()->generateHead()->encode('data-url')}"/>

HTML;
