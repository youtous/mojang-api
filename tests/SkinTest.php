<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 12/11/2016
 * Time: 20:43
 */

namespace MojangAPI\Tests;

use Intervention\Image\ImageManagerStatic;
use MojangAPI\Skin;
use PHPUnit\Framework\TestCase;

class SkinTest extends TestCase
{

    private $Skin;
    /**
     * @before
     */
    public function __construct()
    {
        parent::__construct();
        $this->Skin = new Skin(1000, ImageManagerStatic::make(dirname(__FILE__).'/assets/jeb_.png')->encode('png')->getEncoded(), true);
    }

    public function testHeadWithHelm(){
        $this->assertEquals($this->Skin->generateHead(256, true)->encode('png')->getEncoded(), ImageManagerStatic::make(dirname(__FILE__).'/assets/head-helm.png')->encode('png')->getEncoded(), "Excepted result avatar with helm.");
    }
    public function testHeadNoHelm(){
        $this->assertEquals($this->Skin->generateHead(256, false)->encode('png')->getEncoded(), ImageManagerStatic::make(dirname(__FILE__).'/assets/head-nohelm.png')->encode('png')->getEncoded(), "Excepted result avatar with no helm.");
    }

}