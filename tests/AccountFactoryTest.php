<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 12/11/2016
 * Time: 20:43
 */

namespace MojangAPI\Tests;

use MojangAPI\Account;
use MojangAPI\AccountFactory;
use PHPUnit\Framework\TestCase;

class AccountFactoryTest extends TestCase
{
    public function testCreateByUUIDValid()
    {
        $this->assertInstanceOf(Account::class, AccountFactory::createByUUID('4182c7364d1944a2b4883e40aff5c7e8'), "Account found.");
    }

    public function testCreateByUUIDInvalid()
    {
        $this->expectException(\InvalidArgumentException::class);
        AccountFactory::createByUUID('123', "Invalid UUID.");
    }

    public function testCreateByPlayerNameValid()
    {
        $this->assertInstanceOf(Account::class, AccountFactory::createByPlayerName('youtous'), "Account found.");
    }

    public function testCreateByPlayerNameInvalid()
    {
        $this->expectException(\InvalidArgumentException::class);
        AccountFactory::createByPlayerName('1', "Invalid player name.");
    }

}