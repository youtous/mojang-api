<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 12/11/2016
 * Time: 20:18
 */

namespace MojangAPI;

use Cake\Http\Client;

/**
 * Class AccountFactory
 * Delegate Instantiations of Account.
 *
 * @package MojangAPI
 */
class AccountFactory
{
    /**
     * Instantiate Account of a specified UUID.
     *
     * @param String $uuid - Mojang UUID of the player.
     * @return Account
     * @throws TooManyRequestsException - When the server has sent too many requests.
     */
    public static function createByUUID(String $uuid)
    {
        $resolver = new Resolver(['host' => 'sessionserver.mojang.com']);

        $account = $resolver->get('session/minecraft/profile/' . $uuid);

        if ($account == null) {
            throw new \InvalidArgumentException('Invalid Mojang uuid.');
        }

        // search the textures in proprieties
        $SkinProperties = null;
        foreach ($account['properties'] as $property) {
            if ($property['name'] ?? false == 'textures') {
                $SkinProperties = $property['value'];
                break;
            }
        }

        /**
         * Generate the skin.
         */
        $SkinProperties = base64_decode($SkinProperties);
        if ($SkinProperties == false) {
            throw new \InvalidArgumentException("The Skin texture is not valid.");
        }
        $SkinProperties = json_decode($SkinProperties);
        $SkinImage = null;

        if (isset($SkinProperties->textures->SKIN)) {
            $SkinImage = (new Client())->get($SkinProperties->textures->SKIN->url);
            if ($SkinImage->isOk()) {
                $SkinImage = $SkinImage->body();
            }
        }

        $Skin = new Skin($SkinProperties->timestamp, $SkinImage, $SkinProperties->isPublic ?? true);
        return new Account($account['id'], $account['name'], $Skin);
    }

    /**
     * Instantiate Account of a specified player name.
     *
     * @param String $playerName - Actual player name.
     * @return mixed|Account Account if success, Response object else
     */
    public static function createByPlayerName(String $playerName)
    {
        $uuid = self::resolveUUIDByPlayerName($playerName);

        if ($uuid == null) {
            throw new \InvalidArgumentException("Can't resolve this player name.");
        }

        return self::createByUUID($uuid);
    }

    /**
     * Resolve the Mojang UUID from the player name.
     *
     * @param String $playerName - Actual player name.
     * @return uuid|null uuid if success, null else
     */
    public static function resolveUUIDByPlayerName(String $playerName)
    {
        $resolver = new Resolver();
        $response = $resolver->get('/users/profiles/minecraft/' . $playerName);

        if (isset($response['id'])) {
            return $response['id'];
        } else {
            return null;
        }
    }

}