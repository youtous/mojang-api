<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 12/11/2016
 * Time: 19:41
 */

namespace MojangAPI;

/**
 * Class Account
 * Minecraft Mojang Account.
 * Uses Mojang API.
 * http://wiki.vg/Mojang_API
 *
 * @package MojangAPI
 */
class Account
{
    private $uuid;

    private $playerName;

    private $Skin;

    /**
     * Account constructor.
     *
     * @param String $uuid - Mojang UUID of the account.
     * @param String $playerName - Player name.
     * @param array|\ArrayObject|String $textures - base64 textures
     */
    public function __construct(String $uuid, String $playerName, Skin $Skin)
    {
        $this->uuid = $uuid;
        $this->playerName = $playerName;
        $this->Skin = $Skin;
    }

    /**
     * @return String - UUID.
     */
    public function getUUID()
    {
        return $this->uuid;
    }

    /**
     * @return String - The actual name of the player.
     */
    public function getPlayerName()
    {
        return $this->playername;
    }

    /**
     * Getter of the player's Skin.
     *
     * @see Skin
     *
     * @return Skin
     */
    public function getSkin()
    {
        return $this->Skin;
    }

}