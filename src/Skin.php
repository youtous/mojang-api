<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 12/11/2016
 * Time: 19:57
 */

namespace MojangAPI;

use Intervention\Image\ImageManagerStatic;

/**
 * Class Skin
 * The skin is the texture of the player.
 *
 * @package MojangAPI
 */
class Skin
{
    private $isPublic;
    private $timestamp;
    private $skinImage;

    public function __construct(int $timestamp, String $skinImage, bool $isPublic)
    {
        $this->isPublic = $isPublic;
        $this->timestamp = $timestamp;
        $this->skinImage = $skinImage;
    }

    public function isPublic()
    {
        return $this->isPublic;
    }

    public function getTimestamp()
    {
        return $this->timestamp;
    }

    public function generateHead($size = 256, $helm = true)
    {
        if ($this->skinImage != null) {
            $face = ImageManagerStatic::canvas(8, 8, '#000');

            $face = $face->insert(ImageManagerStatic::make($this->skinImage)->crop(8, 8, 8, 8)); // face
            if ($helm) {
                $face->insert(ImageManagerStatic::make($this->skinImage)->crop(8, 8, 40, 8)); // helm
            }
            return $face->fit($size);
        }

        return false;
    }

}