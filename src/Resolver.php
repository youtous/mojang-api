<?php
/**
 * Created by PhpStorm.
 * User: youtous
 * Date: 12/11/2016
 * Time: 19:17
 */

namespace MojangAPI;


use Cake\Core\InstanceConfigTrait;
use Cake\Http\Client;
use Cake\Network\Exception\HttpException;
use MojangAPI\Exceptions\MojangAPIRequestException;

/**
 * Class Resolver
 *
 * Used as socket to get account's information.
 *
 * @package MojangAPI
 */
class Resolver
{

    use InstanceConfigTrait;

    private $socket;

    /**
     * Default configuration for the resolver.
     *
     * @var array
     */
    protected $_defaultConfig = [
        'timeout' => 30,
        'host' => 'api.mojang.com',
        'scheme' => 'https',
        'port' => 443,
    ];

    /**
     * Resolver constructor.
     *
     * @param array $config - @see Cake\Http\Client config
     * @throws \HttpRequestException - if the api is not reachable;
     */
    public function __construct($config = [])
    {
        $this->config($config);

        $this->socket = new Client($this->_config);

        if (!$this->socket->get('/')->isOk()) {
            throw new HttpException($this->config('host') . " is not reachable.");
        }
    }

    /**
     * Getter of api. Format the result as arrays.
     *
     * @param String $URL - The requesting URL
     * @return Request - the Request result as an array
     * @throws MojangAPIRequestException -
     */
    public function get(String $URL)
    {
        $request = $this->socket->get($URL)->json;
        if(isset($request['error'])) {
            throw new MojangAPIRequestException("Error when requesting the API.");
        }else {
            return $request;
        }
    }
}